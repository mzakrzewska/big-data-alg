import scala.io.Source

def mapStringToListOfInts(s: String) = {
    s.split("\\s+").toList.map((s: String) => s.toInt)
}

def mapFunction(input: List[Int]) : List[List[Int]] = {
    var res = List[List[Int]]()
    res = res :+ List(input(0), 0, 1) :+ List(input(1), 1, 0)
    return res
}

def reduceFunction(X: List[List[Int]], Y: List[List[Int]]) : List[List[Int]] = {
    return add(add(X, Y(0)), Y(1))
}

def add(X: List[List[Int]], y: List[Int]) : List[List[Int]] = {
    var T = List[List[Int]]()
    var found = false
    for (x <- X){
        if(x(0) == y(0)){
            T = T :+ List(x(0), x(1) + y(1), x(2) + y(2))
            found = true
        }
        else{
            T = T :+ x
        }
    }
    if(found != true){
        T = T :+ y
    }
    return T
}

@main def graph: Unit = {

    val graphFilePath = "edges.txt"
    var graph = Source.fromFile(graphFilePath).getLines.toList.map(mapStringToListOfInts)
    var output = graph.map(mapFunction).reduce(reduceFunction)

    output.foreach(line => println("(" + line(0) + ", inDeg(" + line(0) + ") = " + line(1) + ", outDeg(" + line(0) + ") = " + line(2) + "),"))
}
