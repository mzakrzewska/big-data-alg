import scala.collection.immutable.Map
import scala.io.Source
import Sanitizer._

// ==================================================================================   Sanitizer, package
package Sanitizer
{

// ==================================================================================   Input, abstract class 
//                                                                                      loads text from file or from given string
//                                                                                      removes punctuations and stop words, and 
//                                                                                      then saves it to string
    abstract class Input{
        var stopWordsPath = "./books/stop_words_english.txt"
        var stopWords = Source.fromFile(stopWordsPath).getLines.toList
        def returnText: String 
        def sanitize: Seq[String] = {
            // https://stackoverflow.com/questions/37657706/removing-stopwords-from-a-string-in-spark

            val words = returnText.replaceAll("\\p{Punct}", "").replaceAll("[“]", " ").replaceAll("[”]", " ").replaceAll("[’]", "'").replaceAll("[\\d.]", "")
                .toLowerCase()
                .split("\\s+")
                //.filter(!stopWords.contains(_)) // filtering stop words doesn't seem to be desirable here
                .toSeq 
            return words
        }
    }

// ==================================================================================   textFromString, class
//                                                                                      see: Input class, works for strings
    class textFromString(var text:String) extends Input{
        override def returnText: String = {
            return text
        }  
    }

// ==================================================================================   textFromFile, class
//                                                                                      see: Input class, works for files
    class textFromFile(var textPath:String) extends Input{
        override def returnText: String = {
            val text = Source.fromFile(textPath).getLines.mkString
            return text
        }  
    }
}

// ==================================================================================   Corpus, class 
//                                                                                      class containing few documents, able to generate
//                                                                                      k-Shingles and calculate Jaccard similarity between 
//                                                                                      2 selected documents
class Corpus(pathsToFiles: Map[String, String])
{
    //loaded texts
    var docs = Map.empty[String, Seq[String]]

    def loadAllTexts = {
        var Input  = Seq[String]()
        for ((docId, path) <- pathsToFiles){
            val BookInput = new textFromFile(path)
            var doc = BookInput.sanitize
            docs = docs ++ Map(docId -> doc)
        }     
    }

    def createKShingles(docId: String, k: Int) : Set[String] ={
        var dlugiString = docs(docId).mkString(" ")
        var stringLen = dlugiString.length()
        var kShingles = Set[String]()
        var cnt = 0
        while(cnt <= stringLen - k){
            kShingles = kShingles + dlugiString.slice(cnt, cnt + k)
            cnt += 1
        }
        return kShingles
    }

    def Jaccard(docId_A : String, docId_B : String, k: Int) : Float = {
        var A = createKShingles(docId_A, k)
        var B = createKShingles(docId_B, k)
        return A.intersect(B).size.toFloat / A.union(B).size.toFloat
        // return (A & B).size.toFloat / (A | B).size.toFloat // <-- inny zapis
    }
}

object docSimilarity{
    def main(args: Array[String]): Unit = 
    {
        // first 3 are similar, next 3 are different
        val books = Map("doc1" -> "./books/10_wood/1_woodwork_joints.txt",
                        "doc2" -> "./books/10_wood/2_mechanical_properties_of_wood.txt",
                        "doc3" -> "./books/10_wood/3_handwork_in_wood.txt",
                        "doc4" -> "./books/10_diff/1_cooking_and_dining_in_imperial_rome.txt",
                        "doc5" -> "./books/10_diff/2_the_art_of_perfumery.txt",
                        "doc6" -> "./books/10_diff/4_history_of_world_war.txt")
        val booksIds = List("doc1", "doc2", "doc3", "doc4", "doc5", "doc6")

        var corp = new Corpus(books)
        corp.loadAllTexts

        var k = 4
        while(k < 14){
            print("\n\n------------------------------------------------\n\nk = " + k + "\n\n\t")
            booksIds.foreach(docId => print(docId + "\t"))
            print("\n")
            for(docId_A<- booksIds){
                print(docId_A)
                for(docId_B<- booksIds){
                    var value = corp.Jaccard(docId_A, docId_B, k)
                    print("\t" + f"$value%.2f")
                }
                print("\n")
            }
            k += 1
        }


        
    }
}