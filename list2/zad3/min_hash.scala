import scala.collection.mutable.Map
import scala.io.Source
import Sanitizer._
import util.Random.nextInt

// ==================================================================================   Sanitizer, package
package Sanitizer
{

// ==================================================================================   Input, abstract class 
//                                                                                      loads text from file or from given string
//                                                                                      removes punctuations and stop words, and 
//                                                                                      then saves it to string
    abstract class Input{
        var stopWordsPath = "./books/stop_words_english.txt"
        var stopWords = Source.fromFile(stopWordsPath).getLines.toList
        def returnText: String 
        def sanitize: Seq[String] = {
            // https://stackoverflow.com/questions/37657706/removing-stopwords-from-a-string-in-spark

            val words = returnText.replaceAll("\\p{Punct}", "").replaceAll("[“]", " ").replaceAll("[”]", " ").replaceAll("[’]", "'").replaceAll("[\\d.]", "")
                .toLowerCase()
                .split("\\s+")
                .filter(!stopWords.contains(_)) // filtering stop words doesn't seem to be desirable here
                .toSeq 
            return words
        }
    }

// ==================================================================================   textFromString, class
//                                                                                      see: Input class, works for strings
    class textFromString(var text:String) extends Input{
        override def returnText: String = {
            return text
        }  
    }

// ==================================================================================   textFromFile, class
//                                                                                      see: Input class, works for files
    class textFromFile(var textPath:String) extends Input{
        override def returnText: String = {
            val text = Source.fromFile(textPath).getLines.mkString
            return text
        }  
    }
}

// ==================================================================================   Corpus, class 
//                                                                                      class containing few documents, able to generate
//                                                                                      k-Shingles and calculate Jaccard similarity between 
//                                                                                      2 selected documents
class Corpus(pathsToFiles: Map[String, String])
{
    //loaded texts
    var docs = Map[String, Seq[String]]()
    var M = Map[String, List[String]]() //characteristic matrix 
                                        //- Matrix Representation of Sets
    var H = List[List[Int]]() //hash tables - they are pretending to be 
                              //real hash function.Intead of computing h(i), 
                              //you just take random value h(i)
    var Signatures = List[(String, Array[Int])]()

    def loadAllTexts = {
        var Input  = Seq[String]()
        for ((docId, path) <- pathsToFiles){
            val BookInput = new textFromFile(path)
            var doc = BookInput.sanitize
            docs = docs ++ Map(docId -> doc)
        }     
    }

    def createKShingles(docId: String, k: Int) : Set[String] ={
        var dlugiString = docs(docId).mkString(" ")
        var stringLen = dlugiString.length()
        var kShingles = Set[String]()
        var cnt = 0
        while(cnt <= stringLen - k){
            kShingles = kShingles + dlugiString.slice(cnt, cnt + k)
            cnt += 1
        }
        return kShingles
    }

    def createM(k:Int) = {
        for((docId, doc) <- docs){
            var Shingles = createKShingles(docId, k)
            for(s <- Shingles){
                if(M.contains(s)){
                    M.update(s, M(s) ++ List(docId) )
                }
                else{
                    M = M ++ Map(s->List(docId))
                }                
            }
        }
    }

    def generateH = {
        var sortedList = List[Int]()
        var cnt = 0
        while(cnt < M.size){
            sortedList = sortedList :+ cnt
            cnt += 1
        }
        cnt = 0
        while(cnt < 100){
            var h = scala.util.Random.shuffle(sortedList)
            H = H ++ List(h)
            cnt += 1
        }
    }
    
    def minhashAlg = {
        for ((docId, doc) <- docs){
            var cnt = 0
            var initArray = Array[Int]()
            while(cnt < 100){
                initArray = initArray :+ 10000
                cnt += 1
            }
            Signatures = Signatures :+ ((docId, initArray))
        }
        var shingleNo = 0
        for ((shingle, docList) <- M){
            for((docId, sigElems) <- Signatures){
                // println(docId)
                // println(sigElems)
                if(docList.contains(docId)){
                    var i = 0
                    while(i < 100){
                        sigElems(i) = sigElems(i).min(H(i)(shingleNo))
                        i += 1
                    }
                }
            }
            shingleNo += 1
        }
    }

    def checkSimilarity(docId_A : String, docId_B : String) : Float = {
        var A = Array[Int]()
        var B = Array[Int]()
        Signatures.foreach { elem => if (elem._1 == docId_A) A = elem._2 }
        Signatures.foreach { elem => if (elem._1 == docId_B) B = elem._2 }
        var cnt = 0 
        var noOfMatch = 0
        while (cnt < 100){
            if(A(cnt) == B(cnt)){
                noOfMatch += 1
            }
            cnt += 1
        }
        return noOfMatch.toFloat/100
    }

    def Jaccard(docId_A : String, docId_B : String, k: Int) : Float = {
        var A = createKShingles(docId_A, k)
        var B = createKShingles(docId_B, k)
        return A.intersect(B).size.toFloat / A.union(B).size.toFloat
        // return (A & B).size.toFloat / (A | B).size.toFloat // <-- inny zapis
    }
}

object docSimilarity{
    def main(args: Array[String]): Unit = 
    {
        val books = Map("doc1" -> "./books/10_wood/9_woodworking_tools.txt",
                        "doc2" -> "./books/10_wood/5_mission_furniture_1.txt")

        var k = 4
        while(k < 14){
            var corp = new Corpus(books)
            corp.loadAllTexts

            corp.createM(k)    
            corp.generateH
            corp.minhashAlg


            print("\n\n------------------------------------------------\n\nk = " + k + "\n\n\t")
            books.foreach((docId, doc) => print(docId + "\t\t"))
            print("\n")
            for((docId_A, doc) <- books){
                print(docId_A)
                for((docId_B, doc) <- books){
                    var value_J = corp.Jaccard(docId_A, docId_B, k)
                    var value_S = corp.checkSimilarity(docId_A, docId_B)
                    print("\t" + f"$value_J%.2f" + "\t" + f"$value_S%.2f")
                }
                print("\n")
            }
            k += 1
        }  
    }
}